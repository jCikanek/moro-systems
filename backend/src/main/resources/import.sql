INSERT INTO public."user"("name","login","password",salt) VALUES ('Tony Stark','tony','+ts+secure-salt-1+','secure-salt-1');
INSERT INTO public."user"("name","login","password",salt) VALUES ('Captain America','captain','+ca+secure-salt-2+','secure-salt-2');
INSERT INTO public."user"("name","login","password",salt) VALUES ('Black Widow','widow','+bw+secure-salt-3+','secure-salt-3');
INSERT INTO public."user"("name","login","password",salt) VALUES ('Clint Barton','barton','+cb+secure-salt-4+','secure-salt-4');