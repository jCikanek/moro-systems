package net.cikanek.morosystems.rest.user;

import net.cikanek.morosystems.business.Tools;
import net.cikanek.morosystems.business.UserService;
import net.cikanek.morosystems.rest.security.AppPrincipals;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.security.Principal;
import java.util.List;

/**
 * Created by cikanek on 25.01.2018.
 **/

@RestController
@RequestMapping(
        path = SecuredUserController.API_USER,
        consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE},
        produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
public class SecuredUserController {
    static final String API_USER = "/api/secured/user";
    private final UserService userService;

    public SecuredUserController(UserService userService) {
        this.userService = userService;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<DeleteUserDto> deleteUser(
            @Valid
            @Min(0)
            @PathVariable("id") Integer id) {


        if (!userService.deleteUser(id)) return ResponseEntity.notFound().build();

        return ResponseEntity.ok(new DeleteUserDto(id));
    }


    @PostMapping("/")
    public ResponseEntity<UserDto> addUser(@Validated @RequestBody NewUserDto newUserDto, Errors errors) {
        if (errors.hasErrors()) return ResponseEntity.badRequest().varyBy(errors.toString()).build();
        UserDto userDto = userService.addUser(newUserDto);
        return Tools.buildResponseCreated(Tools.toPublicUrl("user", userDto.getId().toString()), userDto);
    }
}
