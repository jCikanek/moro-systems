package net.cikanek.morosystems.rest.user;

public class DeleteUserDto {
    private int id;

    public DeleteUserDto() {
    }

    public DeleteUserDto(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
