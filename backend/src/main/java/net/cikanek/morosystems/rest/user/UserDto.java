package net.cikanek.morosystems.rest.user;

import com.google.common.base.MoreObjects;
import net.cikanek.morosystems.business.Tools;
import net.cikanek.morosystems.model.user.UserDb;

/**
 * Created by cikanek on 26.01.2018.
 **/
public class UserDto {



    private Integer id;
    private String userUrl;
    private String deleteUrl;
    private String name;
    private String login;


    public UserDto() {
    }

    public UserDto(Integer id, String userUrl, String deleteUrl, String name, String login) {
        this.id = id;
        this.userUrl = userUrl;
        this.deleteUrl = deleteUrl;
        this.name = name;
        this.login = login;
    }

    public Integer getId() {
        return id;
    }

    public String getUserUrl() {
        return userUrl;
    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public String getDeleteUrl() {
        return deleteUrl;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", userUrl='" + userUrl + '\'' +
                ", deleteUrl='" + deleteUrl + '\'' +
                ", name='" + name + '\'' +
                ", login='" + login + '\'' +
                '}';
    }
}
