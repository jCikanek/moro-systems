package net.cikanek.morosystems.rest.user;

import net.cikanek.morosystems.business.Tools;
import net.cikanek.morosystems.business.UserService;
import net.cikanek.morosystems.rest.security.AppPrincipals;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.security.Principal;
import java.util.List;

/**
 * Created by cikanek on 25.01.2018.
 **/

@RestController
@RequestMapping(
        path = PublicUserController.API_USER,
        consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE},
        produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
public class PublicUserController {
    static final String API_USER = "/api/public/user";
    private final UserService userService;

    public PublicUserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/")
    public ResponseEntity<List<UserDto>> getAllUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @GetMapping("/status")
    public ResponseEntity<UserStatusDto> getUserStatus(Principal principal) {
        if (principal == null) return ResponseEntity.ok(new UserStatusDto(null, false));

        AppPrincipals appPrincipals = (AppPrincipals) principal;

        Integer id = appPrincipals.getId();


        return ResponseEntity.ok(
                new UserStatusDto(Tools.toPublicUrl("user", id.toString()),true));
    }


    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUserDetails(
            @Valid
            @Min(0)
            @PathVariable("id") Integer id) {

        UserDto userDto = this.userService.getUserById(id);
        if(userDto == null) return ResponseEntity.notFound().build();

        return ResponseEntity.ok(userDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<DeleteUserDto> deleteUser(
            @Valid
            @Min(0)
            @PathVariable("id") Integer id) {


        if(!userService.deleteUser(id)) return ResponseEntity.notFound().build();




        return ResponseEntity.ok(new DeleteUserDto(id));
    }
}
