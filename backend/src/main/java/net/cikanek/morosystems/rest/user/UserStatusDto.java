package net.cikanek.morosystems.rest.user;

public class UserStatusDto {
    private boolean isLogged;
    private String userUrl;

    UserStatusDto(
            String userUrl,
            boolean isLogged) {
        this.userUrl = userUrl;
        this.isLogged = isLogged;
    }

    public String getUserUrl() {
        return userUrl;
    }

    public boolean isLogged() {
        return isLogged;
    }
}
