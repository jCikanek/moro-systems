package net.cikanek.morosystems.rest.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

public class AppPrincipals extends UsernamePasswordAuthenticationToken {
    private final int id;

   public AppPrincipals(
            String userName,
            String password, List<GrantedAuthority> authorities,
            int id) {
        super(userName, password, authorities);
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
