package net.cikanek.morosystems.rest.user;

import com.google.common.base.MoreObjects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * Created by cikanek on 29.01.2018.
 **/
public class NewUserDto {
    @Pattern(regexp = "^[a-zA-Z0-9áčďéěíňóřšťůúýžÁČĎÉĚÍŇÓŘŠŤŮÚÝŽ\\s_-]+$")
    private String name;
    @Pattern(regexp = "^[a-zA-Z0-9áčďéěíňóřšťůúýžÁČĎÉĚÍŇÓŘŠŤŮÚÝŽ\\s_-]+$")
    private String login;
    @NotNull
    private String password;

    public NewUserDto() {
    }

    public NewUserDto(String name, String login, String password) {
        this.name = name;
        this.login = login;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("login", login)
                .add("password", password)
                .toString();
    }
}
