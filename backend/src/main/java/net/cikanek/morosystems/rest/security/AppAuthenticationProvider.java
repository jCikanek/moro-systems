package net.cikanek.morosystems.rest.security;


import com.google.common.collect.Lists;
import net.cikanek.morosystems.business.UserAuthenticator;
import net.cikanek.morosystems.business.UserService;
import net.cikanek.morosystems.config.MasterCredentials;
import net.cikanek.morosystems.model.user.UserDb;
import net.cikanek.morosystems.model.user.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

@Service
public class AppAuthenticationProvider implements AuthenticationProvider {
    private UserAuthenticator userAuthenticator;

    public AppAuthenticationProvider(UserAuthenticator userAuthenticator) {
        this.userAuthenticator = userAuthenticator;
    }


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (StringUtils.isBlank(authentication.getName()) || authentication.getCredentials() == null) return null;
        return  userAuthenticator.authenticate(authentication.getName(), authentication.getCredentials().toString());
    }



    @Override
    public boolean supports(Class<?> authentication) {
        return authentication == UsernamePasswordAuthenticationToken.class;
    }
}
