package net.cikanek.morosystems.model.user;


import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserDb,Integer> {
    UserDb findByLogin(String login);
}
