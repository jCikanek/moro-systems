package net.cikanek.morosystems.model;

import net.cikanek.morosystems.config.ModelConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * Created by cikanek on 26.01.2018.
 **/
@Configuration("net.cikanek.morosystems.model")
@EnableTransactionManagement
public class DbConfig {
//    private static final String CONNECTION_STRING = "jdbc:postgresql://%s:%d/";


    @Primary
    @Bean
    DataSource dataSource(ModelConfig modelConfig){

        DriverManagerDataSource dataSource = new DriverManagerDataSource();


        dataSource.setUrl(modelConfig.getHost());
        dataSource.setUsername(modelConfig.getLogin());
        dataSource.setPassword(modelConfig.getPassword());
        dataSource.setSchema(modelConfig.getDatabase());
        dataSource.setDriverClassName("org.postgresql.Driver");


        return  dataSource;
    }

}
