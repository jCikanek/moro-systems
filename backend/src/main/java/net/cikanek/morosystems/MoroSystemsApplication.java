package net.cikanek.morosystems;

import net.cikanek.morosystems.config.MasterCredentials;
import net.cikanek.morosystems.model.user.UserRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
public class MoroSystemsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoroSystemsApplication.class, args);
    }

    @Bean
    ApplicationRunner applicationRunner(UserRepository userRepository, MasterCredentials masterCredentials) {
        return applicationArguments -> {
            userRepository.findAll().forEach(i -> System.out.println(i.getName()));

            System.out.println(masterCredentials);
        };

    }

}
