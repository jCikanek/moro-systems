package net.cikanek.morosystems.config;

/**
 * Created by cikanek on 26.01.2018.
 **/
public interface SystemVariableProvider {
    String getValue(String name);
}
