package net.cikanek.morosystems.config;

import com.google.common.base.MoreObjects;

/**
 * Created by cikanek on 26.01.2018.
 **/
class MasterCredentialsImpl implements MasterCredentials {
    private String login;
    private String password;

     MasterCredentialsImpl(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("login", login)
                .add("password", password)
                .toString();
    }
}
