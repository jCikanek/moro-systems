package net.cikanek.morosystems.config;

/**
 * Created by cikanek on 26.01.2018.
 **/
public interface MasterCredentials {
    String getLogin();
    String getPassword();
}
