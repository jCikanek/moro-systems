package net.cikanek.morosystems.config;

import org.springframework.stereotype.Service;

/**
 * Created by cikanek on 26.01.2018.
 **/
@Service
class SystemVariableProviderImpl implements SystemVariableProvider {
    @Override
    public String getValue(String name) {
        return System.getenv(name);
    }
}
