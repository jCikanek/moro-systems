package net.cikanek.morosystems.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

/**
 * Created by cikanek on 26.01.2018.
 **/
@Configuration
@PropertySource("classpath:application.properties")
@PropertySource("classpath:config.properties")
public class AppConfig {
    private final SystemVariableProvider variableProvider;

    @Value("${db-host}")
    private String defaultHost;

    @Value("${db-user}")
    private String defaultUser;

    @Value("${db-password}")
    private String defaultPassword;

    @Autowired
    public AppConfig(SystemVariableProvider variableProvider) {
        this.variableProvider = variableProvider;
    }


    @Primary
    @Bean
    ModelConfig modelConfigProduction() {
        String dbHost = variableProvider.getValue("db-host");
        String user = variableProvider.getValue("db-user");
        String password = variableProvider.getValue("db-password");
//        String database = variableProvider.getValue("db-database");


        if (StringUtils.isBlank(dbHost)) dbHost = defaultHost;
        if (StringUtils.isBlank(user)) user = defaultUser;
        if (StringUtils.isBlank(password)) password = defaultPassword;
//        if (StringUtils.isBlank(database)) database = defaultDatabase;


        int defaultPort = 5432;
        String defaultDatabase = "";
        return new ModelConfigImpl(
                dbHost,
                defaultPort,
                defaultDatabase,
                user,
                password);
    }

    @Bean
    MasterCredentials masterLogin(@Value("${master_user}") String masterLogin,@Value("${master_password}") String masterPassword) {
        System.out.printf("Login: " + masterLogin + " p: " + masterPassword);
        return new MasterCredentialsImpl(masterLogin, masterPassword);
    }

}
