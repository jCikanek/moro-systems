package net.cikanek.morosystems.config;

/**
 * Created by cikanek on 26.01.2018.
 **/
public interface ModelConfig {
    String getHost();
    int getPort();
    String getDatabase();
    String getLogin();
    String getPassword();
}
