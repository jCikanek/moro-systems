package net.cikanek.morosystems.config;

import com.google.common.base.MoreObjects;

/**
 * Created by cikanek on 26.01.2018.
 **/
class ModelConfigImpl implements ModelConfig {
    private String host;
    private int port;
    private String database;
    private String login;
    private String password;

    ModelConfigImpl(String host, int port, String database, String login, String password) {
        this.host = host;
        this.port = port;
        this.database = database;
        this.login = login;
        this.password = password;
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public int getPort() {
        return port;
    }

    @Override
    public String getDatabase() {
        return database;
    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("host", host)
                .add("port", port)
                .add("database", database)
                .add("login", login)
                .add("password", password)
                .toString();
    }
}
