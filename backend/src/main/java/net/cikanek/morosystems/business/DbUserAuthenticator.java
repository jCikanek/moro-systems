package net.cikanek.morosystems.business;

import com.google.common.collect.Lists;
import net.cikanek.morosystems.config.MasterCredentials;
import net.cikanek.morosystems.model.user.UserDb;
import net.cikanek.morosystems.model.user.UserRepository;
import net.cikanek.morosystems.rest.security.AppPrincipals;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

@Service
class DbUserAuthenticator implements UserAuthenticator {

    private final UserRepository userRepository;
    private final MasterCredentials masterCredentials;

    public DbUserAuthenticator(UserRepository userRepository, MasterCredentials masterCredentials) {
        this.userRepository = userRepository;
        this.masterCredentials = masterCredentials;
    }


    @Override
    public Authentication authenticate(String login, String password){
        if (StringUtils.isBlank(login) || StringUtils.isBlank(password)) return null;

        if (authenticateByMasterCredentials(login, password))
            return createMasterPrincipals();


        UserDb user = userRepository.findByLogin(login);
        return authenticateByDb(password, user) ? createDbPrincipals(user) : null;
    }

    private Authentication createDbPrincipals(UserDb user) {
        return new AppPrincipals(
                user.getLogin(),
                user.getPassword(),
                Lists.newArrayList((GrantedAuthority) "ROLE_USER"::toUpperCase),
                user.getId());
    }

    private Authentication createMasterPrincipals() {
        return new AppPrincipals(
                masterCredentials.getLogin(),
                masterCredentials.getPassword(),
                Lists.newArrayList((GrantedAuthority) "ROLE_USER"::toUpperCase),
                0);
    }


    private boolean authenticateByDb(String password, UserDb userDb) {
        if(userDb == null) return  false;
        String pwd = Tools.getSaltedPassword(password, userDb.getSalt());
        return pwd.equals(userDb.getPassword());
    }

    private boolean authenticateByMasterCredentials(String login, String password) {
        if (StringUtils.isBlank(masterCredentials.getLogin())
                || StringUtils.isBlank(masterCredentials.getPassword())) return false;

        return masterCredentials.getLogin().equals(login) &&
                masterCredentials.getPassword().equals(password);
    }

}
