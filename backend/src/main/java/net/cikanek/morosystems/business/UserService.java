package net.cikanek.morosystems.business;

import net.cikanek.morosystems.rest.user.NewUserDto;
import net.cikanek.morosystems.rest.user.UserDto;

import java.util.List;

/**
 * Created by cikanek on 26.01.2018.
 **/
public interface UserService {
    List<UserDto> getAllUsers();

    UserDto getUserById(int id);

    boolean deleteUser(Integer id);

    UserDto addUser(NewUserDto newUserDto);
}
