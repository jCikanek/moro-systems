package net.cikanek.morosystems.business;

import org.springframework.security.core.Authentication;

public interface UserAuthenticator {
    Authentication authenticate(String login, String password);
}
