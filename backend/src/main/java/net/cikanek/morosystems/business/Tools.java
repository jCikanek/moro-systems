package net.cikanek.morosystems.business;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by cikanek on 26.01.2018.
 **/
public final class Tools {
    private static final String API_ROOT = "/api/";
    private final static String SALT_TEMPLATE = "+%s+%s+";

    private static String toUrl(String domain, String... parts) {
        if (parts == null) return API_ROOT;
        List<String> args = Arrays.stream(parts)
                .filter(StringUtils::isNotBlank)
                .map(it -> (it.endsWith("/") ? it : it + "/")).collect(Collectors.toList());

        return (API_ROOT + domain + "/" + String.join("", args)).replace("//", "/");
    }

    public static String toPublicUrl(String... parts) {
        return toUrl("public", parts);
    }

    public static String toSecureUrl(String... parts) {
        return toUrl("secured", parts);
    }

    public static <TResult> ResponseEntity<TResult> buildResponseCreated(String url, TResult data) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("location", url)
                .body(data);
    }


    public static String getSaltedPassword(String password, String salt){
        return String.format(SALT_TEMPLATE, password, salt);
    }
}
