package net.cikanek.morosystems.business;

import com.google.common.collect.Lists;
import net.cikanek.morosystems.config.MasterCredentials;
import net.cikanek.morosystems.model.user.UserDb;
import net.cikanek.morosystems.model.user.UserRepository;
import net.cikanek.morosystems.rest.user.NewUserDto;
import net.cikanek.morosystems.rest.user.UserDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by cikanek on 26.01.2018.
 **/
@Service
class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {

        this.userRepository = userRepository;

    }

    private UserDb createMasterUser() {
        return new UserDb(0, "Master User", "master", "", "");
    }

    private static UserDto mapUserToDto(UserDb user) {
        if (user == null) return null;
        return new UserDto(
                user.getId(),
                Tools.toPublicUrl("user", user.getId().toString()),
                Tools.toSecureUrl("user", user.getId().toString()),
                user.getName(),
                user.getLogin());
    }

    @Override
    public List<UserDto> getAllUsers() {
        List<UserDto> result = Lists.newArrayList();
        userRepository.findAll().forEach(it -> result.add(mapUserToDto(it)));
        return result;
    }


    @Override
    public UserDto getUserById(int id) {
        if (id == 0) return mapUserToDto(createMasterUser());

        return mapUserToDto(userRepository.findOne(id));
    }

    @Override
    public boolean deleteUser(Integer id) {
        UserDb userDb = userRepository.findOne(id);
        if(userDb == null) return  false;
        userRepository.delete(userDb);
        return true;
    }

    @Override
    public UserDto addUser(NewUserDto newUserDto) {

        UserDb userDb = new UserDb();
        userDb.setLogin(newUserDto.getLogin());
        userDb.setName(newUserDto.getName());

        String salt = UUID.randomUUID().toString();
        String pwd = Tools.getSaltedPassword(newUserDto.getPassword(), salt);

        userDb.setPassword(pwd);
        userDb.setSalt(salt);
        return mapUserToDto(userRepository.save(userDb));
    }
}
