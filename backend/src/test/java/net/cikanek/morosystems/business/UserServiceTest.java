package net.cikanek.morosystems.business;

import net.cikanek.morosystems.model.user.UserDb;
import net.cikanek.morosystems.model.user.UserRepository;
import net.cikanek.morosystems.rest.user.NewUserDto;
import net.cikanek.morosystems.rest.user.UserDto;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by cikanek on 26.01.2018.
 **/
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepositoryMock;

    private UserService tested;

    private UserDb defaultUser = new UserDb(5, "name", "login", "password", "salt");

    @Before
    public void setUp() throws Exception {

        when(userRepositoryMock.findAll()).thenReturn(Lists.newArrayList(
                defaultUser,
                defaultUser
        ));

        when(userRepositoryMock.findOne(defaultUser.getId())).thenReturn(defaultUser);
        tested = new UserServiceImpl(userRepositoryMock);
    }

    @Test
    public void getAllUsers() {
        List<UserDto> result = tested.getAllUsers();
        assertEquals("Size", 2, result.size());
        verify(userRepositoryMock, times(1)).findAll();
    }

    @Test
    public void getUserById() {
        UserDto result = tested.getUserById(defaultUser.getId());
        assertNotNull("Instance", result);
        verify(userRepositoryMock, times(1)).findOne(defaultUser.getId());
    }

    @Test
    public void getUserByIdUserNotFound() {
        UserDto result = tested.getUserById(8);
        assertNull("Instance", result);
    }

    @Test
    public void mapUserDbToDto() {
        UserDto result = tested.getUserById(defaultUser.getId());
        assertEquals("Id", defaultUser.getId(), result.getId());
        assertEquals("Name", defaultUser.getName(), result.getName());
        assertEquals("Login", defaultUser.getLogin(), result.getLogin());
        assertEquals("Url", Tools.toPublicUrl("user", "5"), result.getUserUrl());
        assertEquals("Url", Tools.toSecureUrl("user", "5"), result.getDeleteUrl());
    }

    @Test
    public void deleteUserSuccess() {
        boolean result = tested.deleteUser(defaultUser.getId());
        verify(userRepositoryMock, times(1)).findOne(defaultUser.getId());
        verify(userRepositoryMock, times(1)).delete(defaultUser);
        assertTrue("Found", result);
    }

    @Test
    public void deleteUserFailed() {
        boolean result = tested.deleteUser(8);
        verify(userRepositoryMock, times(1)).findOne(8);
        verify(userRepositoryMock, never()).delete(defaultUser);
        assertFalse("Found", result);
    }

    @Test
    public void addUserDbMapping() {
        NewUserDto dto = new NewUserDto("test", "login", "password");

        tested.addUser(dto);

        ArgumentCaptor<UserDb> captor = ArgumentCaptor.forClass(UserDb.class);

        verify(userRepositoryMock, times(1)).save(captor.capture());

        UserDb dbObj = captor.getValue();
        assertNotNull("Db object instance", dbObj);

        assertEquals(dto.getLogin(), dbObj.getLogin());
        assertEquals(dto.getName(), dbObj.getName());
        String salt = dbObj.getSalt();
        assertNotNull(salt);

        assertEquals(Tools.getSaltedPassword(dto.getPassword(), salt), dbObj.getPassword());
    }

    @Test
    public void addUserResultDto() {
        NewUserDto dto = new NewUserDto("test", "login", "password");
        when(userRepositoryMock.save(any(UserDb.class))).thenReturn(defaultUser);
        UserDto result = tested.addUser(dto);

        assertEquals("Id", defaultUser.getId(), result.getId());
        assertEquals("Name", defaultUser.getName(), result.getName());
        assertEquals("Login", defaultUser.getLogin(), result.getLogin());
        assertEquals("Url", Tools.toPublicUrl("user", "5"), result.getUserUrl());
        assertEquals("Url", Tools.toSecureUrl("user", "5"), result.getDeleteUrl());

    }
}