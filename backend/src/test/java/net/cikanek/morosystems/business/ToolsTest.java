package net.cikanek.morosystems.business;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by cikanek on 26.01.2018.
 **/
public class ToolsTest {

    @Test
    public void toUrl_EmptyString() {
        String res = Tools.toPublicUrl("");
        assertEquals("/api/public/",res);
    }

    @Test
    public void  toUrl_OneArgument() {
        String res =  Tools.toPublicUrl("arg");
        assertEquals("/api/public/arg/",res);
    }

    @Test
    public void  toUrl_IgnoreSlash() {
        String res = Tools.toPublicUrl("arg/");
        assertEquals("/api/public/arg/",res);
    }

    @Test
    public void  toUrl_MultiArgs(){
        String res = Tools.toPublicUrl("arg/","arg2");
        assertEquals("/api/public/arg/arg2/",res);
    }

    @Test
    public void  toUrl_MultiArgsOneStartsWithSlash(){
        String res = Tools.toPublicUrl("arg/","/arg2");
        assertEquals("/api/public/arg/arg2/",res);
    }


    @Test
    public void  toSecuredUrl(){
        String res = Tools.toSecureUrl("arg/","/arg2");
        assertEquals("/api/secured/arg/arg2/",res);
    }
}