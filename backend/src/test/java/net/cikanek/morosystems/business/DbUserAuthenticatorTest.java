package net.cikanek.morosystems.business;

import net.cikanek.morosystems.config.MasterCredentials;
import net.cikanek.morosystems.model.user.UserDb;
import net.cikanek.morosystems.model.user.UserRepository;
import net.cikanek.morosystems.rest.security.AppPrincipals;
import net.cikanek.morosystems.rest.security.StubAuthentication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;

import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)

public class DbUserAuthenticatorTest {
    private static final String DEFAULT_PASS = "dbPasswd";
    private static final String DEFAULT_LOGIN = "login";

    @Mock
    private UserRepository userRepositoryMock;

    private MasterCredentials masterCredentials = new MasterCredentials() {
        @Override
        public String getLogin() {
            return "test";
        }

        @Override
        public String getPassword() {
            return "passwd";
        }
    };

    private UserDb userDb = new UserDb(1, "User Name", DEFAULT_LOGIN, "+dbPasswd+salt-1+", "salt-1");

    private DbUserAuthenticator tested;

    private Authentication authentication = new StubAuthentication(userDb.getLogin(), DEFAULT_PASS);


    @Before
    public void setUp() throws Exception {
        tested = new DbUserAuthenticator(userRepositoryMock, masterCredentials);
        when(userRepositoryMock.findByLogin(userDb.getLogin())).thenReturn(userDb);
    }


    @Test
    public void authenticate_ByDbSuccess() {
        AppPrincipals result = (AppPrincipals) tested.authenticate(DEFAULT_LOGIN, DEFAULT_PASS);
        assertNotNull(result);
    }

    @Test
    public void authenticate_ByDbSuccessIdValue() {
        AppPrincipals result = (AppPrincipals) tested.authenticate(DEFAULT_LOGIN, DEFAULT_PASS);
        assertEquals((int) userDb.getId(), result.getId());
    }

    @Test
    public void authenticate_ByDbFailedPassword() {
        AppPrincipals result = (AppPrincipals) tested.authenticate(DEFAULT_LOGIN, "---");
        assertNull(result);
        verify(userRepositoryMock).findByLogin(eq(userDb.getLogin()));
    }

    @Test
    public void authenticate_ByDbFailedLogin() {
        String login = "---";
        AppPrincipals result = (AppPrincipals) tested.authenticate(login, "---");
        assertNull(result);
        verify(userRepositoryMock).findByLogin(eq(login));
    }

    @Test
    public void authenticate_ByMCSuccess() {

        AppPrincipals result = (AppPrincipals) tested.authenticate(masterCredentials.getLogin(), masterCredentials.getPassword());
        assertNotNull(result);
        verify(userRepositoryMock, never()).findByLogin(eq(masterCredentials.getLogin()));
    }

    @Test
    public void authenticate_ByMCPasswordNull() {
        tested = new DbUserAuthenticator(userRepositoryMock, new MasterCredentials() {
            @Override
            public String getLogin() {
                return "MCLogin";
            }

            @Override
            public String getPassword() {
                return null;
            }
        });

        AppPrincipals result = (AppPrincipals) tested.authenticate("MCLogin", "---");
        assertNull(result);
        verify(userRepositoryMock).findByLogin(eq("MCLogin"));
    }

    @Test
    public void authenticate_ByMCLoginNull() {
        tested = new DbUserAuthenticator(userRepositoryMock, new MasterCredentials() {
            @Override
            public String getLogin() {
                return null;
            }

            @Override
            public String getPassword() {
                return "MCPass";
            }
        });

        AppPrincipals result = (AppPrincipals) tested.authenticate("MCLogin", "---");
        assertNull(result);
        verify(userRepositoryMock).findByLogin(eq("MCLogin"));
    }
}