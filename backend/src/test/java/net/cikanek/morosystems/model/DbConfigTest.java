package net.cikanek.morosystems.model;

import net.cikanek.morosystems.config.ModelConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by cikanek on 26.01.2018.
 **/
@RunWith(MockitoJUnitRunner.class)
public class DbConfigTest {
    private final static String DB_HOST = "test";
    private final static int DB_PORT = 1234;
    private final static String USER = "db-user";
    private final static String PASSWORD = "db-password";
    private final static String SCHEMA = "db-schema";

    @Mock
    private ModelConfig modelConfigMock;


    private DbConfig tested;


    @Before
    public void setUp() throws Exception {
        tested = new DbConfig();

        when(modelConfigMock.getHost()).thenReturn(DB_HOST);
        when(modelConfigMock.getPort()).thenReturn(DB_PORT);
        when(modelConfigMock.getLogin()).thenReturn(USER);
        when(modelConfigMock.getPassword()).thenReturn(PASSWORD);
        when(modelConfigMock.getDatabase()).thenReturn(SCHEMA);
    }

//    @Test
//    public void dataSource() {
//        DriverManagerDataSource ds = (DriverManagerDataSource)tested.dataSource(modelConfigMock);
//
//        assertEquals("URL","jdbc:postgresql://"+DB_HOST+":"+DB_PORT+"/",ds.getUrl());
//        assertEquals("User",USER,ds.getUsername());
//        assertEquals("Password",PASSWORD,ds.getPassword());
//        assertEquals("Database",SCHEMA,ds.getSchema());
//
//    }
}