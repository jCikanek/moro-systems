package net.cikanek.morosystems.config;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * Created by cikanek on 26.01.2018.
 **/

@SpringBootTest(classes = {AppConfig.class,SystemVariableProviderImpl.class})
@RunWith(SpringRunner.class)
@TestPropertySource(properties = {
        "master_user = master-login",
        "master_password=master-passwd",
        "spring.datasource.url=[prop-host]",
        "spring.datasource.username=[prop-user]",
        "spring.datasource.password=[prop-password]"
})
public class AppConfigTest {
    @Mock
    private SystemVariableProvider variableProviderMock;

    @Autowired
    private MasterCredentials masterCredentials;

    @Autowired
    private ModelConfig modelConfig;


    @Before
    public void setUp() throws Exception {
        when(variableProviderMock.getValue(eq("db-host"))).thenReturn("[db-host]");
        when(variableProviderMock.getValue(eq("db-user"))).thenReturn("[db-user]");
        when(variableProviderMock.getValue(eq("db-password"))).thenReturn("[db-password]");
        when(variableProviderMock.getValue(eq("db-database"))).thenReturn("[db-database]");
    }

    @Test
    public void modelConfig_NotNull() {
        assertNotNull(modelConfig);
    }

    @Test
    public void modelConfig_DefaultValues() {
        assertEquals("host","[prop-host]",modelConfig.getHost());
        assertEquals("user","[prop-user]",modelConfig.getLogin());
        assertEquals("password","[prop-password]",modelConfig.getPassword());
        assertEquals("database","",modelConfig.getDatabase());
    }

    @Test
    public void modelConfig_EnvValues() {
        modelConfig = new AppConfig(variableProviderMock).modelConfigProduction();

        assertEquals("db-host","[db-host]",modelConfig.getHost());
        assertEquals("db-user","[db-user]",modelConfig.getLogin());
        assertEquals("db-password","[db-password]",modelConfig.getPassword());
        assertEquals("db-database","[db-database]",modelConfig.getDatabase());
    }


    @Test
    public void masterLogin_BeanCreated() {
        assertNotNull(masterCredentials);
    }

    @Test
    public void masterLogin_BeanValues() {
        assertEquals("master-login",masterCredentials.getLogin());
        assertEquals("master-passwd",masterCredentials.getPassword());
    }
}