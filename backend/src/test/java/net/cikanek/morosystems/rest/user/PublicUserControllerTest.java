package net.cikanek.morosystems.rest.user;

import com.google.common.collect.Lists;
import net.cikanek.morosystems.business.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.Assert.*;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.Mockito.when;

/**
 * Created by cikanek on 26.01.2018.
 **/
@RunWith(MockitoJUnitRunner.class)
public class PublicUserControllerTest {
    @Mock
    UserService userServiceMock;


    private PublicUserController tested;

    private UserDto defaultDto = new UserDto(7,"/api/user/7/","/api/user/7/","tested","test");

    @Before
    public void setUp() throws Exception {
        when(userServiceMock.getAllUsers()).thenReturn(Lists.newArrayList(defaultDto,defaultDto));

        tested = new PublicUserController(userServiceMock);
    }

    @Test
    public void allUsersStatusCode() throws Exception {
        ResponseEntity<List<UserDto>> responseEntity = tested.getAllUsers();
        then(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void allUsersValues() throws Exception {
        ResponseEntity<List<UserDto>> responseEntity = tested.getAllUsers();
        assertEquals("size",2,responseEntity.getBody().size());
    }


}