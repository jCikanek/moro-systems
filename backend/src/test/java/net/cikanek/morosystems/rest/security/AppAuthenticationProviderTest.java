package net.cikanek.morosystems.rest.security;

import com.google.common.collect.Lists;
import net.cikanek.morosystems.business.UserAuthenticator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AppAuthenticationProviderTest {
    private static final String DEFAULT_PASS = "dbPasswd";
    private static final String DEFAULT_LOGIN = "login";


    @Mock
    private UserAuthenticator userAuthenticatorMock;

    private AppAuthenticationProvider tested;

    private AppPrincipals appPrincipals = new AppPrincipals("userName","password", Lists.newArrayList(),5);

    private StubAuthentication credentials = new StubAuthentication(DEFAULT_LOGIN,DEFAULT_PASS);

    @Before
    public void setUp() throws Exception {
        tested = new AppAuthenticationProvider(userAuthenticatorMock);
        when(userAuthenticatorMock.authenticate(DEFAULT_LOGIN,DEFAULT_PASS)).thenReturn(appPrincipals);
    }

    @Test
    public void verifyValues() {
        tested.authenticate(credentials);
        verify(userAuthenticatorMock).authenticate(eq(DEFAULT_LOGIN),eq(DEFAULT_PASS));
    }

    @Test
    public void verifyResponse() {
        Authentication result = tested.authenticate(credentials);
        assertEquals("Object are not same",appPrincipals,result);
    }

    @Test
    public void credentialsMissingLogin() {
        tested.authenticate(new StubAuthentication(null,"xxx"));
        verify(userAuthenticatorMock,never()).authenticate(eq(DEFAULT_LOGIN),eq(DEFAULT_PASS));
    }


    @Test
    public void credentialsMissingPassword() {
        tested.authenticate(new StubAuthentication("xxx",""));
        verify(userAuthenticatorMock,never()).authenticate(eq(DEFAULT_LOGIN),eq(DEFAULT_PASS));
    }


}