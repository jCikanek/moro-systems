const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        index: "./src/app/index.tsx",
        model: "./src/app/model/Storage.ts"
    },
    output: {
        filename: "[name].bundle.js",
        path: __dirname + "/build/"
    },
    plugins: [
        new CleanWebpackPlugin(['build']),
        new HtmlWebpackPlugin({
            title: 'Production',
            template: "./src/index.html"
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'common' // Specify the common bundle's name.
        }),
        // new webpack.ProvidePlugin({
        //     $: "jquery",
        //     jquery: "jQuery",
        //     "windows.jQuery": "jquery",
        //     bootstrapjs: "bootstrap"
        // }),
        new CopyWebpackPlugin([
            {from: './src/assets/jquery-3.2.1.slim.min.js', to: 'assets/jquery-3.2.1.slim.min.js'},
            {from: './src/assets/popper.min.js', to: 'assets/popper.min.js'},
            {from: './src/assets/react-redux-toastr.min.css', to: 'assets/react-redux-toastr.min.css'},
            {from: './src/assets/bootstrap.min.js', to: 'assets/bootstrap.min.js'},
            {from: './src/assets/react-select.css', to: 'assets/react-select.css'}

        ])
    ],

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json"]
    },

    module: {
        rules: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            {
                test: /\.tsx?$/,
                loader: "awesome-typescript-loader"
            },

            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader"
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    }
};