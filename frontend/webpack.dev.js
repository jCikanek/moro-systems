const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    // output: {
    //     // filename: "bundle.js",
    //     path: __dirname + "/build/"
    // },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: 'build',
        compress: true,
        port: 8000,
        // hot: true,
        // inline: true,
        proxy: {
            "/api": "http://localhost:8080",
            "/logout": "http://localhost:8080"
        }
    }
});