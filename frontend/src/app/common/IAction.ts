/**
 * Created by cikanek on 19.12.2017.
 **/
import {Action} from "redux";

export interface IAction<TPayload> extends Action {
    payload?: TPayload
}