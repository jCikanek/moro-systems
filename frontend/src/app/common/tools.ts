export function pad2(number: number): string {
    return (number < 10 ? '0' : '') + number
}

export function durationToStr(duration: number) {
    const h = Math.floor(duration / 3600);
    duration = (duration % 3600);
    const m = Math.floor(duration / 60);
    duration = (duration % 60);
    const s = Math.floor(duration);

    return pad2(h) + ":" + pad2(m) + ":" + pad2(s);
}

export function dateToStr(value: Date) {
    const date = dateToArray(value);
    return pad2(date[2]) + "." + pad2(date[1]) + "." + pad2(date[0]);
}


export function dateToArray(value: Date): number[] {
    return [
        value.getFullYear(),
        value.getMonth() + 1,
        value.getDate()];
}

export function dateFromArray(value: number[]): Date {
    if (!value || value.length < 3) return null;
    return new Date(value[0], value[1] - 1, value[2]);
}


export function durationToArray(duration: number): number[] {
    const h = Math.floor(duration / 3600);
    duration = (duration % 3600);
    const m = Math.floor(duration / 60);
    duration = (duration % 60);
    const s = Math.floor(duration);
    return [h, m, s];
}

export function durationFromArray(value: number[]): number {
    return (value[0] * 3600) + (value[1] * 60) + value[2];
}

export const emailRegularExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
