/**
 * Created by cigi on 1/28/18.
 **/

import * as React from "react";
import {ReactNode} from "react";

interface DialogHeaderProps {
    label?: string;
    onClose: () => void;
}

class DialogHeader extends React.Component<DialogHeaderProps, {}> {
    private handleClose(){
        if(this.props.onClose)
            this.props.onClose();
    }

    render() {
        return (
            <div className="card-header">
                {this.props.label||""}
                <span className="close" onClick={this.handleClose.bind(this)}>&times;</span>
            </div>
        );
    }
}



interface LoginDialogProps {
    label?: string;
    show: boolean;
    onClose: () => void;
    // children?: ReactNode;
}

export class Dialog extends React.Component<LoginDialogProps, {}> {
    private handleClose() {
        if (this.props.onClose)
            this.props.onClose();
    }

    render() {
        const style = {
            display: (this.props.show ? "block" : "none")
        };

        return (
            <div id="myModal" className="modal" style={style}>
                <div className="modal-content  col-8 offset-2 ">
                    <div className="row card">
                        <DialogHeader label={this.props.label} onClose={this.handleClose.bind(this)}/>
                        {this.props.children}
                        {/*<div className="card-block">*/}
                    </div>
                </div>
            </div>
        );
    }
}
