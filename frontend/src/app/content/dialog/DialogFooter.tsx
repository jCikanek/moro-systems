/**
 * Created by cigi on 1/28/18.
 **/

import * as React from "react";

export class DialogFooter extends React.Component<{}, {}> {
    render() {
        return (
            <div className="card-footer text-muted">
                {this.props.children}
            </div>
        );
    }
}
