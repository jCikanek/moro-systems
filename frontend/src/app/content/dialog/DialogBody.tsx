/**
 * Created by cigi on 1/28/18.
 **/

import * as React from "react";

export class DialogBody extends React.Component<{}, {}> {
    render() {
        return (
            <div className="card-body">
                {this.props.children}
            </div>
        );
    }
}
