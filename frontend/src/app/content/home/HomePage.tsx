/**
 * Created by cigi on 1/27/18.
 **/

import * as React from "react";
import {Navbar} from "./layout/navbar/Navbar";
import {connect} from "react-redux";
import {IStorageState} from "../../model/Storage";
import {GetUserStatus} from "../../actions/user/userStatus/GetUserStatus";
import {List} from "./content/List";
import {AvengerStorage} from "../../model/avengers/AvengerStorage";
import {GetAll} from "../../actions/user/all/GetAll";
import {UserStorage} from "../../model/userStorage/UserStorage";
import {LoginDialog} from "./layout/login/LoginDialog";
import {HideLoginDialog} from "../../actions/user/loginDialog/LoginDialogActions";
import {Credentials} from "../../actions/networking/NetworkAction";
import {UserStatusDto} from "../../actions/user/userStatus/UserStatusDto";
import {GetUserDetails} from "../../actions/user/userDetails/GetUserDetails";
import {DeleteUser} from "../../actions/user/delete/DeleteUser";
import ReduxToastr from "react-redux-toastr";
import {NewUserDialog} from "./content/NewUserDialog";
import {NewUserDto} from "../../actions/user/newUser/NewUserDto";
import {NewUser} from "../../actions/user/newUser/NewUser";

interface HomeProps {
    avengers: AvengerStorage[];
    user: UserStorage;
}

interface State {
    showNewUserDialog: boolean;
}

class HomePageCmp extends React.Component<HomeProps, State> {
    constructor(args: any, states: any) {
        // noinspection TypeScriptValidateTypes
        super(args, states);

        this.state = {
            showNewUserDialog: false
        };

        HomePageCmp.getUserStatus();
        if (!this.props.avengers)
            new GetAll().execute();


    }

    private static getUserStatus(credentials?: Credentials) {
        new GetUserStatus(credentials)
            .setNextAction<UserStatusDto>(dto => {
                if (!dto.logged) return null;
                return new GetUserDetails(dto.userUrl)
            })
            .execute();
    }

    private handleCancelDialog() {
        new HideLoginDialog().execute();
    }

    private handleLoginDialog(credentials: Credentials) {
        this.handleCancelDialog();
        HomePageCmp.getUserStatus(credentials);
    }

    private handleDeleteUser(url: string) {
        new DeleteUser(url).execute();
    }

    private handleCancelNewUserDialog() {
        const state: State = {...this.state} as State;
        state.showNewUserDialog = false;
        this.setState(state);
    }

    private handleShowNewUserDialog() {
        const state: State = {...this.state} as State;
        state.showNewUserDialog = true;
        this.setState(state);
    }

    private handleSubmitNewUserDialog(dto: NewUserDto) {
        new NewUser(dto).setNextAction(() => {
            this.handleCancelNewUserDialog();
            return null;
        }).execute();
    }

    render() {
        return (
            <div className="">
                <ReduxToastr
                    timeOut={4000}
                    newestOnTop={false}
                    preventDuplicates
                    position="bottom-right"
                    transitionIn="fadeIn"
                    transitionOut="fadeOut"
                    options={{removeOnHover: true, showCloseButton: true}}/>

                <LoginDialog
                    show={this.props.user.showLoginDialog}
                    onCancel={this.handleCancelDialog.bind(this)}
                    onLogin={this.handleLoginDialog.bind(this)}/>

                <NewUserDialog
                    show={this.state.showNewUserDialog}
                    onCancel={this.handleCancelNewUserDialog.bind(this)}
                    onSubmit={this.handleSubmitNewUserDialog.bind(this)}/>

                <Navbar user={this.props.user}/>
                <br/>
                <div className="container">
                    <div className=" card">
                        <div className="card-header">
                            Avengers
                            &nbsp;
                            {this.props.user.isLogged &&
                            <button className="btn btn-sm btn-outline-dark fa fa-plus"
                                    onClick={this.handleShowNewUserDialog.bind(this)}/>}
                        </div>
                        <div className="card-block">
                            <List avengers={this.props.avengers}
                                  allowDelete={this.props.user.isLogged}
                                  onDelete={this.handleDeleteUser.bind(this)}/>
                        </div>

                    </div>

                </div>
            </div>
        );
    }

}

const mapStateToProps = (state: IStorageState): HomeProps => ({
    avengers: state.avengers.all,
    user: state.user
});


export const HomePage = connect<HomeProps>(mapStateToProps)(HomePageCmp);