/**
 * Created by cigi on 1/24/18.
 **/

import * as React from "react";
import {Link} from "react-router-dom";
import {render} from "react-dom";
import {UserStorage} from "../../../../model/userStorage/UserStorage";
import {ShowLoginDialog} from "../../../../actions/user/loginDialog/LoginDialogActions";
import {UserLogout} from "../../../../actions/user/logout/UserLogout";

interface NavProps {
    user: UserStorage;
}

export class Navbar extends React.Component<NavProps, {}> {
    private handleShowLogin() {
        new ShowLoginDialog().execute();
    }

    private handleLogout() {
        new UserLogout().execute();
    }


    private renderLoginBtn() {
        return (
            <button className="btn btn-sm btn-outline-light my-2 my-sm-0 fa fa-sign-in"
                    type="button"
                    onClick={this.handleShowLogin.bind(this)}/>
        );
    }

    private renderUserDetails() {
        return (

            <button className="btn btn-sm btn-outline-light my-2 my-sm-0 fa fa-sign-out"
                    type="button"
                    onClick={this.handleLogout.bind(this)}/>
        );
    }


    private renderLoginStatus() {
        const user = this.props.user;


        return (
            <div className="my-2 my-lg-0">
                 <span className="navbar-text">
                {user.fullName || ""}&nbsp;
                 </span>
                {user.isLogged ? this.renderUserDetails() : this.renderLoginBtn()}

            </div>
        );
    }


    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark" role="navigation">
                <Link className="navbar-brand" to="/">Avengers</Link>

                {/*<div className="navbar-header">*/}
                {/*<button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/}
                {/*<span className="sr-only">Toggle navigation</span>*/}
                {/*<span className="icon-bar"/>*/}
                {/*<span className="icon-bar"/>*/}
                {/*<span className="icon-bar"/>*/}
                {/*</button>*/}
                {/*</div>*/}

                <div className="navbar-default sidebar" role="navigation">
                    <div className="sidebar-nav navbar-collapse">
                        <ul className="nav" id="side-menu">
                            <li>
                            </li>
                        </ul>
                    </div>
                    {this.renderLoginStatus()}

                </div>

            </nav>
        );
    }
}
