/**
 * Created by cigi on 1/28/18.
 **/

import * as React from "react";
import {Dialog} from "../../../dialog/Dialog";
import {DialogBody} from "../../../dialog/DialogBody";
import {DialogFooter} from "../../../dialog/DialogFooter";
import {Credentials} from "../../../../actions/networking/NetworkAction";
import {ValidationError} from "./Error";

interface LoginDialogProps {
    show: boolean;
    onCancel: () => void;
    onLogin: (credentials: Credentials) => void;
}

interface State {
    // show:boolean,
    login: string;
    loginError: ValidationError;
    password: string
    passwordError: ValidationError;
}


export class LoginDialog extends React.Component<LoginDialogProps, State> {
    constructor(props: LoginDialogProps, state: State) {
        // noinspection TypeScriptValidateTypes
        super(props);

        this.state = {
            // show: false,
            login: "",
            loginError: new ValidationError(),
            password: "",
            passwordError: new ValidationError()
        }

    }

    private handleCloseClick() {
        if (this.props.onCancel) this.props.onCancel();
    }

    render() {
        const errorClasses = "label label-danger spend-validation-alert ";

        return (
            <Dialog label="Přihlášení uživatele"
                    show={this.props.show}
                    onClose={this.handleCloseClick.bind(this)}>
                <DialogBody>
                    <div className="form-group">
                        <label className="control-label">Login

                        </label>
                        <input type="text" className="form-control"
                               id="user-name"
                               name="email"
                            // ng-model="vm.credential.email"
                               placeholder="Login"
                               value={this.state.login}
                               onChange={this.onLoginChange.bind(this)}
                        />


                        <span className={errorClasses + (this.state.loginError.showError ? "" : "hidden")}>
                                {this.state.loginError.errorMSg}
                                </span>

                    </div>
                    <div className="form-group">
                        <label className="control-label">Heslo

                        </label>
                        <input type="password" name="password" className="form-control" id="user-password"
                               placeholder="Heslo"
                               value={this.state.password}
                               onChange={this.onPasswordChange.bind(this)}/>
                    </div>
                    <span className={errorClasses + (this.state.passwordError.showError ? "" : "hidden")}>
                                {this.state.passwordError.errorMSg}
                                </span>
                </DialogBody>
                <DialogFooter>
                    <button className="btn btn-primary" type="button" onClick={this.executeLogin.bind(this)}>
                        Přihlásit
                    </button>
                    &nbsp;
                    <button className="btn btn-primary" type="button" onClick={this.handleCloseClick.bind(this)}>
                        Zrušit
                    </button>
                </DialogFooter>
            </Dialog>
        );
    }

    private onLoginChange(args: React.FormEvent<HTMLInputElement>) {
        const s: State = Object.assign({}, this.state);
        s.login = args.currentTarget.value;
        this.setState(s as any);
    }

    private onPasswordChange(args: React.FormEvent<HTMLInputElement>) {
        const s: State = Object.assign({}, this.state);
        s.password = args.currentTarget.value;
        this.setState(s as any);
    }


    private validate(): boolean {
        const s: State = Object.assign({}, this.state);
        let isOk = true;

        if (!this.state.login) {
            s.loginError = new ValidationError("Není zadaná platná e-mailová adresa.");
            isOk = false;
        }

        if (!this.state.login) {
            s.passwordError = new ValidationError("Heslo musí být zadáno")
            isOk = false;
        }


        this.setState(s as any);
        return isOk;
    }


    private executeLogin() {
        if (!this.validate()) return;

        const credentials = new Credentials(this.state.login, this.state.password);

        if (this.props.onLogin) this.props.onLogin(credentials);

        // new GetUserStatus(credentials).setNextAction<UserStatusDto>(this.getUserDetails).execute();
        // this.hide();
    }


}
