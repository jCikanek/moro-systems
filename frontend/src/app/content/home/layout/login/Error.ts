export class ValidationError {
    constructor(errorMSg: string = null) {
        this._errorMSg = errorMSg;
        this._showError = !!errorMSg;
    }

    private _errorMSg: string;

    get errorMSg(): string {
        return this._errorMSg;
    }

    private _showError: boolean = false;

    get showError(): boolean {
        return this._showError;
    }
}
