/**
 * Created by cikanek on 29.01.2018.
 **/

import * as React from "react";
import {Field, FormErrors, InjectedFormProps, reduxForm, WrappedFieldProps} from "redux-form";
import {DialogBody} from "../../dialog/DialogBody";
import {Dialog} from "../../dialog/Dialog";
import {DialogFooter} from "../../dialog/DialogFooter";
import {NewUserDto} from "../../../actions/user/newUser/NewUserDto";

interface NewUserDialogProps {
    show: boolean;
    onCancel: () => void;
    onSubmit: (dto: NewUserDto) => void;
}

interface FormData {
    login?: string,
    name?: string,
    password?: string,
}

interface RenderFormFieldArgs extends WrappedFieldProps {
    type: string;
    placeholder: string;
    label: string;
    disabled: boolean
}


class NewUserDialogCmp extends React.Component<NewUserDialogProps & InjectedFormProps<FormData, NewUserDialogProps>, {}> {
    static validate(values: FormData): FormErrors<FormData> {
        const errors: FormErrors<FormData> = {};

        if (!values.name) {
            errors.name = 'Jméno musí být zadáno'
        } else if (values.name.length > 255) {
            errors.name = 'Jméno nesmí být delší než 255 znaků'
        }

        if (!values.login) {
            errors.login = 'Login musí být zadán'
        } else if (values.login.length > 255) {
            errors.login = 'Login nesmí být delší než 255 znaků'
        }

        if (!values.password) {
            errors.password = 'Heslo musí být zadáno'
        } else if (values.password.length > 255) {
            errors.password = 'Heslo nesmí být delší než 255 znaků'
        }
        return errors
    }

    private inputField(args: RenderFormFieldArgs) {
        return (
            <div className="form-group">
                {args.label && <label>{args.label}</label>}
                <input className="form-control"
                       {...args.input}
                       placeholder={args.placeholder}
                       type={args.type}
                       disabled={args.disabled}/>
                {args.meta.error &&
                <span className="badge badge-danger">{args.meta.error}</span>}
            </div>
        );
    }

    render() {
        return (
            <Dialog label="Založení uživatele"
                    show={this.props.show}
                    onClose={this.handleCloseClick.bind(this)}>
                <form className="" onSubmit={this.props.handleSubmit(this.submit.bind(this))}>
                    <DialogBody>
                        <Field
                            name="name"
                            component={this.inputField}
                            type="text"
                            label="Jméno"
                            placeholder="Zadejete jméno"/>

                        <Field
                            name="login"
                            component={this.inputField}
                            type="text"
                            label="Login"
                            placeholder="Zadejete login"/>

                        <Field
                            name="password"
                            component={this.inputField}
                            type="password"
                            label="Heslo"
                            placeholder="Zadejete heslo"/>
                    </DialogBody>
                    <DialogFooter>
                        <button className="btn btn-default" type="button" onClick={this.handleCloseClick.bind(this)}>
                            Zrušit
                        </button>
                        &nbsp;
                        <button className="btn btn-primary" type="submit" disabled={this.props.invalid}>
                            Založit uživatele
                        </button>
                    </DialogFooter>
                </form>
            </Dialog>
        );
    }

    private handleCloseClick() {
        if (this.props.onCancel)
            this.props.onCancel();
    }

    private submit(data: FormData) {
        const dto: NewUserDto = {
            name: data.name,
            login: data.login,
            password: data.password
        };

        if (this.props.onSubmit)
            this.props.onSubmit(dto);
    }
}

export const NewUserDialog = reduxForm<FormData, NewUserDialogProps>({
    form: 'NewUserDialog',
    enableReinitialize: true,
    validate: NewUserDialogCmp.validate
})(NewUserDialogCmp);

