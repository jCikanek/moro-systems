/**
 * Created by cigi on 1/28/18.
 **/

import * as React from "react";
import {AvengerStorage} from "../../../model/avengers/AvengerStorage";

interface ColumnProps {
    value: string;
}

class Column extends React.Component<ColumnProps, {}> {
    render() {
        return (
            <td className="">
                {this.props.value}
            </td>
        );
    }
}

interface ActionsProps {
    onDelete:()=>void;
}

class ActionColumn extends React.Component<ActionsProps, {}> {
    private handleDelete(){
        if(this.props.onDelete) this.props.onDelete();
    }

    render() {
        return (
            <td className="">
                <button className="btn btn-sm btn-outline-dark fa fa-remove" onClick={this.handleDelete.bind(this)}/>
            </td>
        );
    }
}


interface RowProps {
    avenger: AvengerStorage;
    allowDelete: boolean;
    onDelete:(url:string)=>void;
}

class Row extends React.Component<RowProps, {}> {
    private handleDelete(){
        if(this.props.onDelete) this.props.onDelete(this.props.avenger.userUrl)
    }

    render() {
        const avenger = this.props.avenger;

        return (
            <tr className="">
                <Column value={avenger.id.toLocaleString()}/>
                <Column value={avenger.name}/>
                <Column value={avenger.login}/>
                {this.props.allowDelete && <ActionColumn onDelete={this.handleDelete.bind(this)}/>}
            </tr>
        );
    }
}


interface BodyProps {
    avengers: AvengerStorage[];
    allowDelete: boolean;
    onDelete:(url:string)=>void;
}

export class ListBody extends React.Component<BodyProps, {}> {
    private handleDelete(url:string){
        if(this.props.onDelete) this.props.onDelete(url)
    }

    render() {
        const rows = (this.props.avengers || [])
            .map((it, key) =>
                <Row key={key} avenger={it} allowDelete={this.props.allowDelete} onDelete={this.handleDelete.bind(this)}/>);


        return (
            <tbody className="">
            {rows}
            </tbody>
        );
    }
}
