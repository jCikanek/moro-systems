/**
 * Created by cigi on 1/28/18.
 **/

import * as React from "react";

interface HeaderProps {
    headers: string[];
    allowDelete:boolean;
}

export class Header extends React.Component<HeaderProps, {}> {
    render() {
        const headerItems = (this.props.headers || []).map((it, key) => <th key={key}>{it}</th>);

        return (
            <thead className="thead-dark">
            <tr>
                {headerItems}
                {this.props.allowDelete && <th>&nbsp;</th>}
            </tr>

            </thead>
        );
    }
}
