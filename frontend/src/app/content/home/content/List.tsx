/**
 * Created by cigi on 1/27/18.
 **/

import * as React from "react";
import {Header} from "./ListHeader";
import {ListBody} from "./ListBody";
import {AvengerStorage} from "../../../model/avengers/AvengerStorage";

interface ListProps{
    avengers: AvengerStorage[];
    allowDelete:boolean
    onDelete:(url:string)=>void;
}

export class List extends React.Component<ListProps, {}> {
    private handleDelete(url:string){
        if(this.props.onDelete) this.props.onDelete(url)
    }

    render() {
        return (
            <table className="table">
                <Header headers={["#","Jméno","Login"]} allowDelete={this.props.allowDelete}/>
                <ListBody
                    avengers={this.props.avengers}
                    allowDelete={this.props.allowDelete}
                    onDelete={this.handleDelete.bind(this)}/>
            </table>
        );
    }
}