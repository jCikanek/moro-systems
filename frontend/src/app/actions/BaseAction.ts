/**
 * Created by cikanek on 10.01.2018.
 **/
import {IAction} from "../common/IAction";
import getStorage from "../model/Storage";


export class BaseAction<TPayload> {
    private payload: TPayload;
    private type: string;

    constructor(type: string, payload: TPayload = null) {
        this.payload = payload;
        this.type = type;
    }

    public execute() {
        if (!this.type) return;
        const action: IAction<TPayload> = {
            type: this.type,
            payload: this.payload
        };
        getStorage().dispatch(action);
    }
}