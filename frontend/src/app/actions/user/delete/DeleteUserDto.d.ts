/**
 * Created by cikanek on 15.01.2018.
 **/
export interface DeleteUserDto {
    id: number;
}