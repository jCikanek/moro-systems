import {DeleteNetworkAction} from "../../networking/NetworkAction";
import {DeleteUserDto} from "./DeleteUserDto";


export const DELETE_USER = "DELETE_USER";

export class DeleteUser extends DeleteNetworkAction<DeleteUserDto> {

    constructor(url: string) {
        super(DELETE_USER, url);
    }
}
