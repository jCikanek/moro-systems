export interface UserDto {
    id: number,
    userUrl: string,
    deleteUrl: string,
    name: string,
    login: string
}