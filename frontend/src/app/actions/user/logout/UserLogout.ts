import {PostNetworkAction} from "../../networking/NetworkAction";



export const USER_LOGOUT = "USER_LOGOUT";
const USER_LOGOUT_URL = "/logout";

export class UserLogout extends PostNetworkAction<{}> {

    constructor() {
        super(USER_LOGOUT, USER_LOGOUT_URL);
    }
}
