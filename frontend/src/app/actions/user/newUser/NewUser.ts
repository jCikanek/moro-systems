import {PostNetworkAction} from "../../networking/NetworkAction";
import {NewUserDto} from "./NewUserDto";
import {UserDto} from "../UserDto";


export const NEW_USER = "NEW_USER";
const NEW_USER_URL = "/api/secured/user/";

export class NewUser extends PostNetworkAction<UserDto> {

    constructor(data: NewUserDto) {
        super(NEW_USER, NEW_USER_URL);
        this.setData(data);
    }
}
