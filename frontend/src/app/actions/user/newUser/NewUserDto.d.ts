/**
 * Created by cikanek on 29.01.2018.
 **/

export interface NewUserDto {
    name:string;
    login:string;
    password:string;
}
