export interface UserStatusDto {
    userUrl:string;
    logged: boolean;
}