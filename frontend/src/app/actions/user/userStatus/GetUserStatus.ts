import {UserStatusDto} from "./UserStatusDto";
import {Credentials, GetNetworkAction} from "../../networking/NetworkAction";


export const GET_USER_STATUS = "GET_USER_STATUS";
const USER_STATUS_URL = "/api/public/user/status";

export class GetUserStatus extends GetNetworkAction<UserStatusDto> {

    constructor(credentials?: Credentials) {
        super(GET_USER_STATUS, USER_STATUS_URL, credentials);
    }
}
