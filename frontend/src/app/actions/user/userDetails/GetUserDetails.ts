import {GetNetworkAction} from "../../networking/NetworkAction";
import {UserDto} from "../UserDto";


export const GET_USER_DETAILS = "GET_USER_DETAILS";

export class GetUserDetails extends GetNetworkAction<UserDto> {

    constructor(url: string) {
        super(GET_USER_DETAILS, url);
    }
}
