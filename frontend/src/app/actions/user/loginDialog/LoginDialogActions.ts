/**
 * Created by cikanek on 20.12.2017.
 **/
import {BaseAction} from "../../BaseAction";


export const SHOW_LOGIN_DIALOG = "SHOW_LOGIN_DIALOG";
export const HIDE_LOGIN_DIALOG = "HIDE_LOGIN_DIALOG";

export class ShowLoginDialog extends BaseAction<{}> {

    constructor() {
        super(SHOW_LOGIN_DIALOG,{});
    }
}

export class HideLoginDialog extends BaseAction<{}> {

    constructor() {
        super(HIDE_LOGIN_DIALOG,null);
    }
}