import {GetNetworkAction} from "../../networking/NetworkAction";
import {UserDto} from "../UserDto";

export const GET_ALL_USERS = "GET_ALL_USERS";
const ALL_USERS_URL = "/api/public/user/";

export class GetAll extends GetNetworkAction<UserDto[]> {

    constructor() {
        super(GET_ALL_USERS, ALL_USERS_URL);
    }
}
