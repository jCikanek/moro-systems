/**
 * Created by cikanek on 20.12.2017.
 **/

export interface SpringError{
    timestamp: number,
    status: number,
    error: string,
    message: string,
    path: string
}
