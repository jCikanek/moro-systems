/**
 * Created by cikanek on 19.12.2017.
 **/
import {Dispatch} from "react-redux";
import getStorage from "../../model/Storage";
import {IAction} from "../../common/IAction";
import {Action} from "redux";
import {toastr} from "react-redux-toastr";
import Axios, {AxiosResponse, AxiosRequestConfig} from "axios";
import {SpringError} from "./SpringError";

export const WEB_REQUEST = "WEB_REQUEST";

export enum RequestState {
    Start = "Start",
    Complete = "Complete",
    Error = "Error"
}

export interface WebRequestPayload {
    "uuid": string;
    "url": string;
    "state": RequestState
    "actionType": string
}

interface DefualtHeaders {
    "X-Requested-With"?: string
    "authorization"?: string
    "Content-Type": string

}

export class Credentials {
    private _login: string;
    private _password: string;


    constructor(login: string, password: string) {
        this._login = login;
        this._password = password;
    }


    get login(): string {
        return this._login;
    }

    get password(): string {
        return this._password;
    }
}

export interface INetworkAction<S> {
    execute<TResult>(): void;

    setNextAction<TResult>(onNext: (data: TResult) => NetworkAction<{}>): INetworkAction<{}>;
}

class NetworkAction<S> implements INetworkAction<S> {
    private _dispatcher: Dispatch<S>;
    private _actionType: string;
    private uuid: string = this.guid();
    private isDefaultErrorHandlerEnabled: boolean = true;
    private customErrorActionType: string = null;
    private _url: string;
    private _credentials: Credentials;
    private _httpMethod: string;
    private data: any = null;

    constructor(httpMethod: string, actionType: string, url: string, credentials?: Credentials) {
        this._url = url;
        this._credentials = credentials;
        this._dispatcher = getStorage().dispatch;
        this._actionType = actionType;
        this._httpMethod = httpMethod;
    }

    private guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return s4() + '-' + s4();
    }

    private webRequest(url: string, state: RequestState): IAction<WebRequestPayload> {
        return {
            "type": WEB_REQUEST,
            "payload": {
                "uuid": this.uuid,
                url: url,
                state: state,
                "actionType": this._actionType
            }
        };
    }


    private createDefaultRequest(actionType: string, data: {}): IAction<{}> {
        return {
            "type": actionType,
            "payload": data
        };
    }

    private createErrorToast(title: string, message: string): Action {
        toastr.error(title, message, {removeOnHover: true, showCloseButton: true});
        return null;
    }


    private handleRequestSuccess<TResult>(url: string, response: AxiosResponse<TResult>) {
        this._dispatcher(this.createDefaultRequest(this._actionType, response.data));
        this._dispatcher(this.webRequest(url, RequestState.Complete))
    }

    private handleRequestError(url: string, data: any) {
        this._dispatcher(this.webRequest(url, RequestState.Error));
        if (this.customErrorActionType)
            this._dispatcher(this.createDefaultRequest(this.customErrorActionType, data));

        if (this.isDefaultErrorHandlerEnabled)
            this.buildDefaultError(data, url);
    }

    public disableDefaultError(): NetworkAction<S> {
        this.isDefaultErrorHandlerEnabled = false;
        return <NetworkAction<S>>this;
    }

    public customErrorAction(actionType: string): NetworkAction<S> {
        this.isDefaultErrorHandlerEnabled = false;
        return <NetworkAction<S>>this;
    }

    protected setData(data: any) {
        this.data = data;
    }

    private nextAction: (data: {}) => INetworkAction<{}>;

    public setNextAction<TResult>(onNext: (data: TResult) => INetworkAction<{}>): INetworkAction<{}> {
        // this.isDefaultErrorHandlerEnabled = false;
        this.nextAction = onNext;
        return <INetworkAction<S>>this;

    }

    public execute<TResult>() {
        const url = this._url;
        this._dispatcher(this.webRequest(url, RequestState.Start));
        Axios.request(this.configure())
            .then((response: AxiosResponse<TResult>) => {
                this.handleRequestSuccess<TResult>(url, response);
                this.executeNextAction(response.data);


            })
            .catch((error) => {
                this.handleRequestError(url, error.response)
            });
    }

    private executeNextAction(data: {}) {
        if (!this.nextAction) return;
        const action = this.nextAction(data);
        if (action) action.execute();

    }

    public configure(): AxiosRequestConfig {
        let defaultHeaders: DefualtHeaders = {
            "X-Requested-With": "XMLHttpRequest",
            "Content-Type": "application/json;charset=UTF-8"
        };

        if (this._credentials) {
            defaultHeaders.authorization = "Basic "
                + btoa(this._credentials.login + ":" + this._credentials.password);
        }


        return {
            url: this._url,
            headers: defaultHeaders,
            method: this._httpMethod,
            data: this.data ? JSON.stringify(this.data) : null
        };

        //
    }


    private buildDefaultError(response: AxiosResponse<SpringError>, url: string = "") {
        const status = response.status;
        let message = "(" + status + ":" + response.statusText + ") ";
        let title = "WebRequest error";
        if (response.data) {
            title = response.data.error;
            message += response.data.message + response.data.path;
        } else
            message += url;

        this.createErrorToast(title, message)
    }
}


export class GetNetworkAction<S> extends NetworkAction<S> {

    constructor(actionType: string, url: string, credentials?: Credentials) {
        super("GET", actionType, url, credentials);
    }
}

export class PostNetworkAction<S> extends NetworkAction<S> {
    constructor(actionType: string, url: string, credentials?: Credentials) {
        super("POST", actionType, url, credentials);
    }
}

export class DeleteNetworkAction<S> extends NetworkAction<S> {
    constructor(actionType: string, url: string, credentials?: Credentials) {
        super("DELETE", actionType, url, credentials);
    }
}

export class UpdateNetworkAction<S> extends NetworkAction<S> {
    constructor(actionType: string, url: string, credentials?: Credentials) {
        super("PUT", actionType, url, credentials);
    }
}
