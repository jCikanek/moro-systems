import {IAction} from "../../common/IAction";
import {AvengersStorage} from "./AvengersStorage";
import {UserDto} from "../../actions/user/UserDto";
import {AvengerStorage} from "./AvengerStorage";
import {GET_ALL_USERS} from "../../actions/user/all/GetAll";
import {DELETE_USER} from "../../actions/user/delete/DeleteUser";
import {DeleteUserDto} from "../../actions/user/delete/DeleteUserDto";
import {NEW_USER} from "../../actions/user/newUser/NewUser";

const defaultValue: AvengersStorage = {};

export function AvengersStorageReducer(oldState: AvengersStorage = defaultValue, action: IAction<{}>) {
    let state: AvengersStorage = {...oldState} as AvengersStorage;

    const mapUsers = function (dto: UserDto): AvengerStorage {
        return {
            name: dto.name,
            id: dto.id,
            login: dto.login,
            userUrl: dto.userUrl
        } as AvengerStorage;
    };


    const mapAllUsers = function (state: AvengersStorage, dtos: UserDto[]): AvengersStorage {
        state.all = (dtos || []).map(it => mapUsers(it));
        return state;
    };

    const deleteUser = function (state: AvengersStorage, dto: DeleteUserDto): AvengersStorage {
        state.all = state.all.filter(it => it.id != dto.id);
        return state;
    };

    const addUser = function (state: AvengersStorage, dto: UserDto): AvengersStorage {
        state.all = [].concat(state.all);
        state.all.push(mapUsers(dto));
        return state;
    };

    switch (action.type) {
        case GET_ALL_USERS:
            return mapAllUsers(state, action.payload as UserDto[]);

        case DELETE_USER:
            return deleteUser(state, action.payload as DeleteUserDto);

        case NEW_USER:
            return addUser(state, action.payload as UserDto);

        default:
            return oldState
    }
}