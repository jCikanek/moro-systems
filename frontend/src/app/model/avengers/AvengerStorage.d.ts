
export interface AvengerStorage{
    id: number,
    name:string;
    userUrl: string,
    login: string
}