import {applyMiddleware, combineReducers, createStore, Store} from "redux";
import promiseMiddleware from "redux-promise-middleware";
import thunk from "redux-thunk";
import logger from "redux-logger";
import {UserStorage} from "./userStorage/UserStorage";
import {UserReducer} from "./userStorage/UserStorageReducer";
import {reducer as toastrReducer} from "react-redux-toastr";
import {reducer as formReducer} from "redux-form"
import {AvengersStorageReducer} from "./avengers/AvengersReducer";
import {AvengersStorage} from "./avengers/AvengersStorage";


export interface IStorageState {
    user: UserStorage;
    avengers:AvengersStorage;
}

const reducers = combineReducers<IStorageState>({
    "user": UserReducer,
    "avengers":AvengersStorageReducer,

    "toastr": toastrReducer,
    "form": formReducer
});

const storage = createStore<IStorageState>(reducers, applyMiddleware(logger, thunk, promiseMiddleware()));

storage.subscribe(() => {

});

export default function getStorage(): Store<IStorageState> {
    return storage;
}