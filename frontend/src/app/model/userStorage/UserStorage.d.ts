

export interface UserStorage {
    isLogged: boolean
    id?: number,
    fullName?: string
    userUrl?:string;
    showLoginDialog:boolean;
}
