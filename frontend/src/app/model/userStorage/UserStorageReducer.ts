/**
 * Created by cikanek on 14.12.2017.
 **/
import {UserStorage} from "./UserStorage";
import {IAction} from "../../common/IAction";
import {GET_USER_STATUS} from "../../actions/user/userStatus/GetUserStatus";
import {UserStatusDto} from "../../actions/user/userStatus/UserStatusDto";
import {HIDE_LOGIN_DIALOG, SHOW_LOGIN_DIALOG} from "../../actions/user/loginDialog/LoginDialogActions";
import {USER_LOGOUT} from "../../actions/user/logout/UserLogout";
import {GET_USER_DETAILS} from "../../actions/user/userDetails/GetUserDetails";
import {UserDto} from "../../actions/user/UserDto";
import {NEW_USER} from "../../actions/user/newUser/NewUser";

const defaultValue: UserStorage = {
    id: null,
    isLogged: false,
    fullName: "",
    showLoginDialog: false
};

export function UserReducer(oldState: UserStorage = defaultValue, action: IAction<{}>) {
    let state: UserStorage = {...oldState} as UserStorage;

    const mapUserStatus = function (state: UserStorage, dto: UserStatusDto): UserStorage {
        state.isLogged = dto.logged;
        state.userUrl = dto.userUrl;
        return state;
    };

    const mapUserDetails = function (state: UserStorage, dto: UserDto): UserStorage {
        state.userUrl = dto.userUrl;
        state.fullName = dto.name;
        state.id = dto.id;
        return state;
    };


    const doUserLogout = function (state: UserStorage): UserStorage {
        state.isLogged = false;
        state.userUrl = null;
        state.fullName = "";
        state.id = null;
        return state;
    };


    switch (action.type) {
        case GET_USER_STATUS:
            return mapUserStatus(state, action.payload as UserStatusDto);

        case SHOW_LOGIN_DIALOG:
            state.showLoginDialog = true;
            return state;

        case HIDE_LOGIN_DIALOG:
            state.showLoginDialog = false;
            return state;

        case USER_LOGOUT:
            return doUserLogout(state);

        case GET_USER_DETAILS:
            return mapUserDetails(state, action.payload as UserDto );


        default:
            return oldState
    }
}