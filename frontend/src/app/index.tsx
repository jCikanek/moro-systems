import * as React from 'react';
import * as ReactDOM from 'react-dom';

import {Provider} from "react-redux";
import {HashRouter} from "react-router-dom";
import {Route, Switch} from "react-router";
import getStorage from "./model/Storage";
//CSS
import '../scss/app.css';
import {HomePage} from "./content/home/HomePage";


let root = document.getElementById('root');
ReactDOM.render(
    (
        <Provider store={getStorage()}>
            <HashRouter>
                <Switch>
                    <Route exact path="/" component={HomePage}/>
                    {/*<Route exact path="/" component={HomePage}/>*/}
                </Switch>
            </HashRouter>
        </Provider>

    ), root);
